#!/usr/bin/env python3
"""
Author: Rob van den Berg
Purpose: Say hello
"""


import argparse


def get_args():
    """Get command-line argumenst"""
    parser = argparse.ArgumentParser(description="Say hello")
    parser.add_argument(
        "-n", "--name", metavar="name", default="World", help="Name to greet"
    )
    return parser.parse_args()


def main():
    """main"""
    args = get_args()
    name = args.name
    print(f"Hello, {name}!")


if __name__ == "__main__":
    main()
