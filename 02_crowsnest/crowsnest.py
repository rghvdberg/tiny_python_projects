#!/usr/bin/env python3
"""
Author : Rob van den Berg
Date   : 2024-05-07
Purpose: Announce the appearance of something "off the larboard bow" 
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Announce the appearance of something "off the larboard bow"',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument("noun", metavar="str", help="noun to announce")

    return parser.parse_args()


def get_article(noun):
    """
    return 'an' if the noun starts with A,E,I,O,U
    else return 'a'
    """
    return "an" if noun[0] in "AEIOUaeiou" else "a"


# --------------------------------------------------
def main():
    """main"""
    args = get_args()
    noun = args.noun
    article = get_article(noun)
    print(f"Ahoy, Captain, {article} {noun} off the larboard bow!")


# --------------------------------------------------
if __name__ == "__main__":
    main()
