# Tiny Python Projects

My code for [Tiny Python Projects](https://tinypythonprojects.com/)

- [x] Getting started: Introduction and installation guide
- [x] 1 How to write and test a Python program
- [ ] 2 The crow’s nest: Working with strings
- [ ] 3 Going on a picnic: Working with lists
- [ ] 4 Jump the Five: Working with dictionaries
- [ ] 5 Howler: Working with files and STDOUT
- [ ] 6 Words count: Reading files and STDIN, iterating lists, formatting strings
- [ ] 7 Gashlycrumb: Looking items up in a dictionary
- [ ] 8 Apples and Bananas: Find and replace
- [ ] 9 Dial-a-Curse: Generating random insults from lists of words
- [ ] 10 Telephone: Randomly mutating strings
- [ ] 11 Bottles of Beer Song: Writing and testing functions
- [ ] 12 Ransom: Randomly capitalizing text
- [ ] 13 Twelve Days of Christmas: Algorithm design
- [ ] 14 Rhymer: Using regular expressions to create rhyming words
- [ ] 15 The Kentucky Friar: More regular expressions
- [ ] 16 The Scrambler: Randomly reordering the middles of words
- [ ] 17 Mad Libs: Using regular expressions
- [ ] 18 Gematria: Numeric encoding of text using ASCII values
- [ ] 19 Workout of the Day: Parsing CSV files, creating text table output
- [ ] 20 Password strength: Generating a secure and memorable password
- [ ] 21 Tic-Tac-Toe: Exploring state
- [ ] 22 Tic-Tac-Toe redux: An interactive version with type hints
- [ ] Epilogue
- [ ] Appendix. Using argparse
